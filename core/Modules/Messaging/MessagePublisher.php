<?php

namespace TBMS\Messaging;

use TBMS\Config\Config;
use TBMS\Requests\FormRequest;
use TBMS\Telegram\APIResponse;
use TBMS\Telegram\TelegramBotAPI;

/**
 * Class MessagePublisher
 *
 * @package TBMS\MessagePublisher
 */
class MessagePublisher
{
    /**
     * Publishes a message.
     *
     * @param string                     $message
     * @param \TBMS\Requests\FormRequest $request
     *
     * @return array
     */
    public static function publish(string $message, FormRequest $request)
    {
        $API = new TelegramBotAPI($request->token);

        $sendResults = self::sendMessages(
            $API,
            $message,
            $request->target
        );

        $forwardResults = [];
        if ($sendResults[0]->status === APIResponse::STATUS_OK) {
            $forwardResults = self::forwardMessages(
                $API,
                $request->target[0],
                $sendResults[0]->result->message_id,
                $request->forward,
                $request->silent
            );
        }

        return [
            'sendResults'    => $sendResults,
            'forwardResults' => $forwardResults,
        ];
    }

    /**
     * Sends a message to one or more chats.
     *
     * @param \TBMS\Telegram\TelegramBotAPI $API
     * @param string                        $message
     * @param array                         $targetChats
     * @param bool                          $silent
     *
     * @return APIResponse[]
     */
    private static function sendMessages(TelegramBotAPI $API, string $message, array $targetChats, bool $silent = false)
    {
        $results = [];
        foreach ($targetChats as $targetChat) {
            $results[] = $API->sendMessage(
                $message,
                $targetChat,
                $silent
            );
            self::sleep();
        }
        return $results;
    }

    /**
     * Waits some time.
     */
    private static function sleep()
    {
        usleep(Config::$MS_TO_WAIT_BETWEEN_API_CALLS);
    }

    /**
     * Forwards a message to one or more chats.
     *
     * @param \TBMS\Telegram\TelegramBotAPI $API
     * @param string                        $sourceChat
     * @param string                        $originalMessageId
     * @param array                         $targetChats
     * @param bool                          $silent
     *
     * @return APIResponse[]
     */
    private static function forwardMessages(TelegramBotAPI $API, string $sourceChat, string $originalMessageId, array $targetChats, bool $silent = false)
    {
        $results = [];
        foreach ($targetChats as $targetChat) {
            $results[] = $API->forwardMessage(
                $sourceChat,
                $originalMessageId,
                $targetChat,
                $silent
            );
            self::sleep();
        }
        return $results;
    }
}
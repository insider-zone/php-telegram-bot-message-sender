<?php

namespace TBMS\Messaging;

use TBMS\Requests\FormRequest;

/**
 * Class MessageBuilder
 *
 * @package TBMS\Messaging
 */
class MessageBuilder
{
    /**
     * Returns a Telegram Bot API compatible message.
     *
     * @param \TBMS\Requests\FormRequest $request
     *
     * @return string
     */
    public static function build(FormRequest $request)
    {
        $message = '';
        if (self::isValid($request->embeddedUrl)) {
            $message .= self::getEmbeddedURLMarkdown(
                $request->embeddedUrl
            );
        }
        if (self::isValid($request->title)) {
            $message .= self::getTitleMarkdown($request->title);
        }
        if (self::isValid($request->content)) {
            $message .= $request->content;
        }
        return $message;
    }

    /**
     * Checks if $var is valid.
     *
     * @param mixed $var
     *
     * @return bool
     */
    private static function isValid($var)
    {
        return $var !== NULL && strlen($var) > 0;
    }

    /**
     * Returns markdown of embedded image.
     *
     * @param string $imageUrl
     *
     * @return string
     */
    private static function getEmbeddedURLMarkdown(string $imageUrl)
    {
        return self::getLinkMarkdown(
            '​​​​​​​​​​​', self::getMarkdownSafeURL($imageUrl)
        );
    }

    /**
     * Returns a hyperlink markdown.
     *
     * @param string $text
     * @param string $url
     *
     * @return string
     */
    private static function getLinkMarkdown(string $text, string $url)
    {
        return '[' . $text . '](' . $url . ')';
    }

    /**
     * Returns the $url with escaped markdown.
     *
     * @param string $url
     *
     * @return string
     */
    private static function getMarkdownSafeURL(string $url)
    {
        $url = self::fixWhitespaces($url);

        $url = str_replace('*', '%2A', $url);
        $url = str_replace('_', '%5f', $url);
        $url = str_replace('[', '%5B', $url);
        $url = str_replace(']', '%5D', $url);
        $url = str_replace('(', '%28', $url);
        $url = str_replace(')', '%29', $url);
        $url = str_replace('`', '%60', $url);

        return $url;
    }

    /**
     * Replaces every whitespace characters with a normal space character.
     *
     * @param string $text
     *
     * @return string|null
     */
    private static function fixWhitespaces(string $text)
    {
        return preg_replace('[^\S\x0a\x0d]', ' ', $text);
    }

    /**
     * Returns title markdown.
     *
     * @param string $title
     *
     * @return string
     */
    private static function getTitleMarkdown(string $title)
    {
        return self::getBoldMarkdown(
                self::getMarkdownSafeText($title)
            ) . PHP_EOL;
    }

    /**
     * Returns a bold $text markdown.
     *
     * @param string $text
     *
     * @return string
     */
    private static function getBoldMarkdown(string $text)
    {
        return '*' . $text . '*';
    }

    /**
     * Returns the $text with escaped markdown.
     *
     * @param string $text
     *
     * @return string
     */
    private static function getMarkdownSafeText(string $text)
    {
        $text = self::fixWhitespaces($text);

        $text = str_replace('*', '∗', $text);
        $text = str_replace('_', ' ', $text);
        $text = str_replace('`', '\'', $text);
        $text = str_replace('](', ']​​​​​​​​​​​(', $text);

        return trim($text);
    }
}
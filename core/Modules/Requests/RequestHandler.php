<?php

namespace TBMS\Requests;

use TBMS\Logs\SimpleLogList;

/**
 * Class RequestHandler
 *
 * @package TBMS\Requests
 */
class RequestHandler
{
    #region CONSTANTS
    #region IMAGE_TYPES
    protected const supportedImageMimeTypes = [
        'image/jpeg',
        'image/png',
        'image/bmp',
    ];
    #endregion
    #region ERROR_MESSAGES
    #region VALIDATION
    protected const eMessageOnValidation        =
        'Problems occurred during processing input data:';
    protected const eMessageOnMissingToken      =
        'Missing required input: API token';
    protected const eMessageOnMissingTargetChat =
        'Missing required input: Target chat(s)';
    #endregion
    #region IMAGE_PARSING
    protected const eMessageOnImageSavingError      =
        'Problems occurred while saving uploaded image!';
    protected const eMessageOnMoveUploadedFileError =
        'move_uploaded_file() exited with error code:';
    #endregion
    #endregion
    #endregion

    /**
     * Parses data from the $_POST and $_FILES variables.
     *
     * @return FormRequest|SimpleLogList
     * FormRequest if request processed successfully,
     * SimpleLogList with reporting on errors.
     */
    public static function parseRequest()
    {
        $request = new FormRequest();
        $logs    = self::validate();

        if ($logs->hasItems()) {
            $logs->prepend(self::eMessageOnValidation);
            return $logs;
        }

        $request->title       = self::parsePlainText($_POST['title']);
        $request->content     = self::parsePlainText($_POST['content']);
        $request->token       = self::parsePlainText($_POST['token']);
        $request->target      = self::parseMultiLineList($_POST['target']);
        $request->forward     = self::parseMultiLineList($_POST['forward']);
        $request->silent      = self::parseBoolean($_POST['silent']);
        $request->embeddedUrl = self::parsePlainText($_POST['url']);

        return $request;
    }

    /**
     * Validates the request before processing data.
     *
     * @return SimpleLogList
     * Empty list if validated successfully,
     * or items containing error messages.
     */
    private static function validate()
    {
        $log = new SimpleLogList();
        if (!isset($_POST['token'])) {
            $log->append(self::eMessageOnMissingToken);
        }
        if (!isset($_POST['target'])) {
            $log->append(self::eMessageOnMissingTargetChat);
        }
        return $log;
    }

    /**
     * Parses HTML text input values.
     *
     * @param string $text
     *
     * @return string|null
     * NULL if $text is not set.
     */
    private static function parsePlainText(string $text)
    {
        if (!isset($text)) {
            return NULL;
        }

        return $text;
    }

    /**
     * Parses lines to an array from HTML textarea input value.
     *
     * @param string $idList
     *
     * @return array|null
     * NULL if $idList in not set.
     */
    private static function parseMultiLineList(string $idList)
    {
        if (!isset($idList)) {
            return NULL;
        }

        $idList  = explode(PHP_EOL, $idList);
        $results = [];
        for ($i = 0; $i < count($idList); $i++) {
            $idList[$i] = trim($idList[$i]);
            if (strlen($idList[$i]) > 1) {
                $results[] = $idList[$i];
            }
        }

        return $results;
    }

    /**
     * Parses a boolean from HTML input value.
     *
     * @param $value
     *
     * @return bool
     */
    private static function parseBoolean($value)
    {
        if (!isset($value)) {
            return false;
        }

        if ($value == 1) {
            return true;
        }

        return false;
    }
}

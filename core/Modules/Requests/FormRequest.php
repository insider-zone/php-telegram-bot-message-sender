<?php

namespace TBMS\Requests;

/**
 * Class FormRequest
 *
 * @property string|NULL $title
 * Optional headline of the message
 *
 * @property string|NULL $content
 * Optional markdown supported message body
 *
 * @property array|NULL  $target
 * Required array of chat ids or handles (at character needed for handles)
 *
 * @property string|NULL $token
 * Required BOT API token
 *
 * @property array|NULL  $forward
 * Optional array of chat ids or handles (at character needed for handles)
 *
 * @property bool|NULL   $silent
 * Optional boolean can disable notifications on forwards
 *
 * @property string|NULL $embeddedUrl
 * Optional URL for embedded image or link preview
 *
 * @package TBMS\Requests
 */
class FormRequest
{
    public $title, $content, $target, $token, $forward, $silent, $embeddedUrl = NULL;
}
<?php

namespace TBMS\Logs;

/**
 * Class SimpleLogList
 *
 * @property array $items
 * Simple array to store log entries.
 *
 * @package TBMS\Logs
 */
class SimpleLogList
{
    public $items = [];

    /**
     * Prepends an item to the list.
     *
     * @param string $item
     */
    public function prepend(string $item)
    {
        array_unshift($this->items, $item);
    }

    /**
     * Appends an item to the list.
     *
     * @param string $item
     */
    public function append(string $item)
    {
        $this->items[] = $item;
    }

    /**
     * Checks if has items in the list.
     *
     * @return bool
     */
    public function hasItems()
    {
        return count($this->items) > 0;
    }

    /**
     * Generates HTML list from log items.
     *
     * @return string
     */
    public function toHTML()
    {
        $html = '<ul>';
        foreach ($this->items as $item) {
            $html .= '<li>' . $item . '</li>';
        }
        return $html . '</ul>';
    }
}
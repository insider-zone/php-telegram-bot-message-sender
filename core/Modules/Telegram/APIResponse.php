<?php

namespace TBMS\Telegram;

/**
 * Class APIResponse
 *
 * @property string|null             $status
 *
 * @property mixed|object|array|null $result
 *
 * @property string|null             $description
 *
 * @package TBMS\Telegram
 */
class APIResponse
{
    public const STATUS_OK    = 'OK';
    public const STATUS_ERROR = 'ERROR';
    public $status, $result, $description = NULL;

    /**
     * APIResponse constructor.
     *
     * @param string|NULL             $status
     * @param mixed|object|array|null $result
     * @param string|null             $description
     */
    public function __construct(string $status = NULL, $result = NULL, string $description = NULL)
    {
        $this->status      = $status;
        $this->result      = $result;
        $this->description = $description;
    }

    /**
     * Returns status string from boolean value.
     *
     * @param bool $status
     *
     * @return string
     */
    public static function booleanToStatus(bool $status)
    {
        if ($status) {
            return self::STATUS_OK;
        }

        return self::STATUS_ERROR;
    }
}
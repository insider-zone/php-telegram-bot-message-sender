<?php

namespace TBMS\Telegram;

/**
 * Class TelegramBotAPI
 *
 * @property string|null $telegramBotAPIToken
 *
 * @package TBMS\Telegram
 */
class TelegramBotAPI
{
    public $telegramBotAPIToken = NULL;

    /**
     * TelegramBotAPI constructor.
     *
     * @param string $telegramBotAPIToken
     */
    public function __construct(string $telegramBotAPIToken)
    {
        $this->telegramBotAPIToken = $telegramBotAPIToken;
    }

    /**
     * Sends message to the target chat.
     *
     * @param string $message
     * @param string $targetChat
     * @param bool   $silent
     *
     * @return \TBMS\Telegram\APIResponse
     */
    public function sendMessage(string $message, string $targetChat, bool $silent = false)
    {
        $command = $this->getAPICommandURL('sendMessage') .
                   '?text=' . urlencode($message) .
                   '&parse_mode=markdown' .
                   '&chat_id=' . urlencode($targetChat) .
                   '&disable_notification=' . $silent;

        return $this->execute($command);
    }

    /**
     * Returns URL of a certain API command.
     *
     * @param string $command
     *
     * @return string
     */
    private function getAPICommandURL(string $command)
    {
        return 'https://api.telegram.org/bot' .
               $this->telegramBotAPIToken .
               '/' . $command;
    }

    /**
     * Executes an API call and returns it's results.
     *
     * @param string $apiCall
     *
     * @return \TBMS\Telegram\APIResponse
     */
    private function execute(string $apiCall)
    {
        $content = file_get_contents($apiCall);
        if ($content !== false) {
            $content = json_decode($content);
            if (json_last_error() === JSON_ERROR_NONE) {
                return $this->getAPIResponse($content);
            }
        }

        return new APIResponse(
            APIResponse::STATUS_ERROR,
            NULL,
            'Failed to get response from the API.'
        );
    }

    /**
     * Creates an APIResponse object from JSON data.
     *
     * @param $jsonData
     *
     * @return \TBMS\Telegram\APIResponse
     */
    private function getAPIResponse($jsonData)
    {
        $APIResponse = new APIResponse();
        if (isset($jsonData->ok)) {
            $APIResponse->status = $APIResponse::booleanToStatus($jsonData->ok);
        }
        if (isset($jsonData->result)) {
            $APIResponse->result = $jsonData->result;
        }
        if (isset($jsonData->description)) {
            $APIResponse->description = $jsonData->description;
        }
        return $APIResponse;
    }

    /**
     * Forwards message from the source chat to the target chat.
     *
     * @param string $sourceChat
     * @param string $originalMessageId
     * @param string $targetChat
     * @param bool   $silent
     *
     * @return \TBMS\Telegram\APIResponse
     */
    public function forwardMessage(string $sourceChat, string $originalMessageId, string $targetChat, bool $silent = false)
    {
        $command = $this->getAPICommandURL('forwardMessage') .
                   '?chat_id=' . urlencode($targetChat) .
                   '&from_chat_id=' . urlencode($sourceChat) .
                   '&disable_notification=' . $silent .
                   '&message_id=' . urlencode($originalMessageId);

        return $this->execute($command);
    }
}
# PHP Telegram Bot Message Sender
PHP TBMS is a micro application to send and auto-forward markdown formatted rich-text messages using a telegram bot.

The project contains a frontend panel to easily compose and broadcast messages.

# Features
- Complete frontend panel
- Embedded image
- Live preview with telegram markdown support
- Post messages to multiple chats like groups and channels
- Auto forward option to broadcast posted message instantly to multiple groups or channels
- Silent forward mode
- Detailed reports

# Requirements
Application requires PHP version 7.1+ and a web server.

# Installation
- Download and copy project files to the web server's directory (/var/www)
- Set up the web server to point to the html directory (/var/www/html)

# Notes
- You can get your API token at Telegram writing to the @botfather.
- Bot must be member of the group or admin of the channel, and must be allowed to post or forward messages to there.

# Security note
Application does not come with user authentication module, so you should secure the installation.

A simple HTTP password authentication is easy to setup, and can give basic level of protection for the most use cases.

# Author credit
Coded and developed by **Milan Patartics**.

Project is licensed under GNU GPL v3.

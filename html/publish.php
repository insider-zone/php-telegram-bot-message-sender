<?php

namespace TBMS;

use TBMS\Logs\SimpleLogList;
use TBMS\Messaging\MessageBuilder;
use TBMS\Messaging\MessagePublisher;
use TBMS\Requests\FormRequest;
use TBMS\Requests\RequestHandler;
use TBMS\Telegram\APIResponse;

require_once __DIR__ . '/../core/autoloader.php';

publish();

function publish()
{
    $request = RequestHandler::parseRequest();
    $class   = get_class($request);
    if ($class === FormRequest::class) {
        $message = MessageBuilder::build($request);
        $results = MessagePublisher::publish($message, $request);

        report(resultsToHTML($request, $results));
    }
    else if ($class === SimpleLogList::class) {
        report($request->toHTML());
    }
}

function resultsToHTML(FormRequest $request, array $results)
{
    $html = '<h3 class="text-primary"><strong>SEND RESULTS: </strong></h3>';
    $html .= '<ul>';
    for ($i = 0; $i < count($results['sendResults']); $i++) {
        $html .= resultHTMLBlock(
            $request->target[$i],
            $results['sendResults'][$i]
        );
    }
    $html .= '</ul>';

    if (count($results['forwardResults'])) {
        $html .= '<br><br><br>';
        $html .= '<h3 class="text-primary"><strong>FORWARD RESULTS: </strong></h3>';
        $html .= '<ul>';
        for ($i = 0; $i < count($results['forwardResults']); $i++) {
            $html .= resultHTMLBlock(
                $request->forward[$i],
                $results['forwardResults'][$i]
            );
        }
        $html .= '</ul>';
    }
    return $html;
}

function resultHTMLBlock(string $chat, APIResponse $results)
{
    $html = '<li><h4 class="text-info">' . $chat . ' - ' . '<strong>' .
            $results->status . '</strong></h4>';
    if ($results->description !== NULL) {
        $html .= '<br><code>' . $results->description . '</code>';
    }
    if ($results->result !== NULL) {
        $html .= '<br><pre>' . json_encode($results->result) . '</pre>';
    }
    return $html . '</li>';
}

function report(string $htmlData)
{
    ?>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Report</title>
        <link rel="stylesheet" href="assets/stylesheets/bootstrap.4.1.2.min.css">
        <link rel="stylesheet" href="assets/stylesheets/style.css">
    </head>
    <body>
    <div class="container results">
        <div class="row">
            <div class="col align-self-center">
                <h1 class="text-primary"><strong>REPORT</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="text-secondary">You can read detailed logs below.</p>
                <hr>
                <?php echo $htmlData; ?>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a class="btn btn-primary btn-lg" role="button" href="index.html">OK</a>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
}
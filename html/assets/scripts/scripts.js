function init() {
    updatePreview();
    updateImage($('#url').val());
    $(document).on(
        'focus blur input change paste keyup mouseenter mouseleave mousedown mouseup',
        '#title, #content', function () {
            updatePreview();
        });

    $(document).on(
        'focus blur change paste mouseleave',
        '#url', function () {
            updateImage($('#url').val());
        });
}

function updatePreview() {
    $('#preview strong').html($('#title').val());
    $('#preview span').html('<br>' + markdownToHTML($('#content').val())).show();
}

function updateImage(src) {
    $('#preview img').attr('src', src);
}

function markdownToHTML(markdown) {
    // hashtag
    markdown = markdown.replace(
        new RegExp('(#[\\D](\\w+))', 'g'),
        '<a href="$1" title="Hashtag: $1">$1</a>'
    );

    // cashtag
    markdown = markdown.replace(
        new RegExp('(^|[^\\d\\w])(\\$([A-Z]+))($|[^\\d\\w])', 'g'),
        '$1<a href="#$2" title="Cashtag: $2">$2</a>$4'
    );

    // @mention
    markdown = markdown.replace(
        new RegExp('(^|\\s)(@[\\D][\\w\\d]+)', 'g'),
        '$1<a href="#$2" title="Mention of $2">$2</a>'
    );

    //  *bold text*
    markdown = markdown.replace(
        new RegExp('(\\*)([^\\*]+)(\\*)', 'g'),
        '<strong>$2</strong>'
    );

    //  _italic text_
    markdown = markdown.replace(
        new RegExp('(_)([^_]+)(_)', 'g'),
        '<em>$2</em>'
    );

    //  [inline mention of a user](tg://user?id=123456789)
    markdown = markdown.replace(
        new RegExp('(\\[)([^\\[\\]]+)(\\])(\\()((tg:\\/\\/user\\?id=)(@)?([^)\\(]+?))(\\))', 'g'),
        '<a href="#$5" title="Mention of @$8">@$8</a>'
    );

    //  [inline URL](http://www.example.com/)
    markdown = markdown.replace(
        new RegExp('(\\[)([^\\[\\]]+)(\\])(\\()([^\\)\\(]+?)(\\))', 'g'),
        '<a href="$5" title="$5" target="_blank">$2</a>'
    );

    // other URLs
    markdown = markdown.replace(
        new RegExp('(^|[^\\(])((http(s)*:\\/\\/)[^\\s)]+)', 'g'),
        '$1<a href="$2" title="$2" target="_blank">$2</a>'
    );

    //  ```block_language
    //  pre-formatted fixed-width code block
    //  ```
    markdown = markdown.replace(
        new RegExp('(`{3})([^`\\s]*(\\s))([^`]+)(`{3})', 'g'),
        '<pre>$4</pre>'
    );

    //  `inline fixed-width code`
    markdown = markdown.replace(
        new RegExp('(`)([^`]+)(`)', 'g'),
        '<code>$2</code>'
    );

    // linebreaks
    markdown = markdown.replace(
        new RegExp('\n', 'g'),
        '<br>'
    );

    return markdown;
}

$(document).ready(function () {
    init();
});